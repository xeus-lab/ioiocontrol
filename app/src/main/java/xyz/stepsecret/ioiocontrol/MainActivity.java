package xyz.stepsecret.ioiocontrol;


import ioio.lib.api.DigitalOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;



public class MainActivity extends IOIOActivity {
	
	private LinearLayout layoutDPad;
	private TextView tvDirection;
	private DPadController dpc;
	
	int direction = DPadController.D_NONE;
	
	private ToggleButton toggleButton0;
	
	private Button button0;
	
	
	
	
	private boolean Go = false;
	private boolean Back = false;
	private boolean Left = false;
	private boolean Right = false;
	
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        toggleButton0 = (ToggleButton) findViewById(R.id.toggleButton0);
        
        
        button0 = (Button) findViewById(R.id.button0);
        
        dpc = new DPadController(R.drawable.dpad_normal
        		, R.drawable.dpad_up, R.drawable.dpad_down
        		, R.drawable.dpad_left, R.drawable.dpad_right);
        
        tvDirection = (TextView)findViewById(R.id.tvDirection);
        
        
        layoutDPad = (LinearLayout)findViewById(R.id.layoutDPad);
        
    }



    
   
    
    
    class Looper extends BaseIOIOLooper {
		
    	private DigitalOutput digital_led0;
    	private DigitalOutput digital_led1;
    	private DigitalOutput digital_led2;
    	private DigitalOutput digital_led3;
    	private DigitalOutput digital_led4;
    	
    	
    	


		@Override
		protected void setup() throws ConnectionLostException, InterruptedException {

			digital_led0 = ioio_.openDigitalOutput(0,true);
			digital_led1 = ioio_.openDigitalOutput(1,false);
			digital_led2 = ioio_.openDigitalOutput(2,false);
			digital_led3 = ioio_.openDigitalOutput(3,false);
			digital_led4 = ioio_.openDigitalOutput(4,false);
			
			
			
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(getApplicationContext(), "IOIO Connect", Toast.LENGTH_SHORT).show();
				}
			});
			
			
			button0.setOnTouchListener(new OnTouchListener()
	        {       
		 @SuppressLint("ClickableViewAccessibility")
		public boolean onTouch(View v, MotionEvent event)  {
	                if(event.getAction() == MotionEvent.ACTION_DOWN) {

	                	
	                	Go = true;

	                } else if (event.getAction() == MotionEvent.ACTION_UP) {

	                	Go = false;
	                }
					return false;
	            }
	            });
			
			
			layoutDPad.setOnTouchListener(new OnTouchListener() {
				@SuppressLint("ClickableViewAccessibility")
				public boolean onTouch(View v, MotionEvent event) {
					dpc.onTouch(v, event);
					direction = dpc.getDirection();
					if(direction == DPadController.D_NONE)
					{
						tvDirection.setText("");
						Go = false;
						Back = false;
						Left = false;
						Right = false;
						
					}
					
					else if(direction == DPadController.D_UP) 
					{
						tvDirection.setText("Up");
						
						Left = true;
						Right = true;
					}
					
					else if(direction == DPadController.D_DOWN) 
					{
						tvDirection.setText("Down");
						Go = true;
						Back = true;
						
					}
					else if(direction == DPadController.D_RIGHT)
					{
						tvDirection.setText("Right");
						Right = true;
						
					}
					else if(direction == DPadController.D_LEFT) 
					{
						tvDirection.setText("Left");
						Left = true;
					}
					
					return true;
				}
			});
			
			
			
		}

		@Override
		public void loop() throws ConnectionLostException {
			
			digital_led0.write(!toggleButton0.isChecked());
			
			digital_led1.write(Go);
			digital_led2.write(Back);
			digital_led3.write(Left);
			digital_led4.write(Right);
			
			
			
				
			try  {
				Thread.sleep(100);
			}catch(InterruptedException e){
				
			}
			
		}
	}

	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}
	
	
    
}
